<?php
/**
 * @file
 * CM Series module, code for cms_template.
 */

/**
 * Implements hook_permission().
 */
function cm_series_permission() {
  return array(
    'admin series_template' => array(
      'title' => t('Administer CM Series Template'),
      'description' => t('Perform administration on CM Series Template.'),
    ),
  );
}

/**
 * Implements hook_access().
 * - workaround for https://www.drupal.org/node/2153463.
 */
function cm_series_template_access($op, $node, $node) {
  if ($op == 'view') {
    return TRUE;
  }
  return user_access('admin cms_template');
}

/**
 * Implements hook_load().
 */
function cms_template_load($template_id) {
  $template = entity_load_single('cms_template', $template_id);
  return $template;
}

/**
 * Form add cms_template.
 */
function cm_series_template_add() {
  $template = entity_create('cms_template', array());
  return drupal_get_form('cm_series_template_form', $template);
}

/**
 * Form cms_template.
 */
function cm_series_template_form($form, &$form_state, $template = NULL) {
  if (!isset($template->is_new)) {
    drupal_set_title($template->label());
  }
  $form_state['cms_template'] = $template;
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Template name'),
    '#default_value' => !empty($template->label) ? $template->label : '',
    '#weight' => 1,
  );

  field_attach_form('cms_template', $template, $form, $form_state);
  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 50,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save template'),
    '#weight' => 100,
  );
  return $form;
}

/**
 * Form validate cms_template.
 */
function cm_series_template_form_validate($form, &$form_state) {
  entity_form_field_validate('cms_template', $form, $form_state);
}

/**
 * Form validate cms_template.
 */
function cm_series_template_form_submit($form, &$form_state) {
  entity_form_submit_build_entity('cms_template', $form_state['cms_template'], $form, $form_state);
  $template = $form_state['cms_template'];
  $template->save();
  drupal_set_message(t('The changes have been saved.'));
  $form_state['redirect'] = 'admin/structure/cm-series-template';
}

/**
 * Delete confirmation form.
 */
function cm_series_template_delete($form, &$form_state, $template) {
  $form_state['cms_template'] = $template;
  return confirm_form($form, t('Are you sure you want to delete template: %title?', array('%title' => $template->label())), 'admin/structure/cm-series-template', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Delete form submit handler.
 */
function cm_series_template_delete_submit($form, &$form_state) {
  $template = $form_state['cms_template'];
  entity_delete('cms_template', $template->id);
  drupal_set_message(t('The template have been deleted.'));
  $form_state['redirect'] = 'admin/structure/cm-series-template';
}
