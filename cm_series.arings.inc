<?php
/**
 * @file
 * CM Series arings.
 */

function cm_series_create_arings($form, &$form_state, $node) {
  // Get all templates
  $query = db_select('cms_template', 't');
  $query->fields('t', array(
    'id',
    'label'
  ));
  $result = $query->execute();
  $templates = $result->fetchAllKeyed();
  $form['shows_markup_start'] = array('#markup' => '<div class="form-item title"><h1>Shows with no arings</h1><p>With selected shows arings will be created using selected template</p></div>');

  $form['table'] = array(
    '#tree' => TRUE,
    '#type' => 'table',
    '#header' => array(
      '' => array('class' => array('select-all')), // For selecting items
      'Titel',
      'Start Date',
      'Template',
    ),
  );
  // For selecting items
  drupal_add_js('misc/tableselect.js');

  if (!empty($node->field_cm_series_episodes[LANGUAGE_NONE])) {
    foreach ($node->field_cm_series_episodes[LANGUAGE_NONE] as $key => $value) {
      $fc = entity_load_single('field_collection_item', $value['value']);
      if (!empty($fc->field_cms_airings[LANGUAGE_NONE])) {
        continue;
      }
      $show = node_load($fc->field_cms_show[LANGUAGE_NONE][0]['target_id']);
      $form['table'][$fc->item_id]['choosen'] = array(
        '#type' => 'checkbox',
        '#return_value' => $fc->item_id,
        '#default_value' => $fc->item_id,
      );
      $form['table'][$fc->item_id]['title'] = array('#markup' => $show->title);
      $date = new DateTime($fc->field_cms_start_date[LANGUAGE_NONE][0]['value'], new DateTimeZone(drupal_get_user_timezone()));
      $form['table'][$fc->item_id]['date'] = array('#markup' => $date->format('Y-m-d'));

      $form['table'][$fc->item_id]['template'] = array(
        '#type' => 'select',
        '#options' => $templates,
        '#default_value' => empty($fc->field_cms_template[LANGUAGE_NONE][0]['target_id']) ? 0 : $fc->field_cms_template[LANGUAGE_NONE][0]['target_id'],
        '#required' => TRUE,
      );
    }
  }
  $form['actions'] = array();
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Arings'),
    '#weight' => 10,
    '#submit' => array('cm_series_create_arings_action'),
    '#validate' => array('cm_series_create_arings_validate'),
  );
  return $form;
}

function cm_series_create_arings_validate($form, &$form_state) {
}

function cm_series_create_arings_action($form, &$form_state) {
  foreach ($form_state['values']['table'] as $fid) {
    if (!empty($fid['choosen'])) {
      _cms_create_arings($fid['choosen'], $fid['template']);
    }
  }
}

/**
 * Implements hook_cm_series_duration_alter().
 *
 * Duration hook_cm_series_duration_alter not implemented, default duration = 0
 */
function cm_series_cm_series_duration_alter(&$show_duration, $show) {
  $show_duration = 0;
}

/**
 * Implements hook_cm_series_channel_alter().
 *
 * Duration hook_cm_series_channel_alter not implemented, default is first channel in taxonomy channel
 */
function cm_series_cm_series_channel_alter(&$channel, $show) {
  $vocabulary = taxonomy_vocabulary_machine_name_load('channel');
  $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
  $channels = array();
  foreach ($terms as $value) {
    $channels[] = $value->tid;
  }
  if (!empty($channels)) {
    $channel = $channels[0];
  }
}

function _cms_create_arings($fid, $templateid = FALSE) {
  $fc = entity_load_single('field_collection_item', $fid);
  if (FALSE === $templateid) {
    if (!empty($fc->field_cms_template[LANGUAGE_NONE][0]['target_id'])) {
      $templateid = $fc->field_cms_template[LANGUAGE_NONE][0]['target_id'];
    }
  }
  // Check if cms_template exist.
  if ($cms_template = entity_load_single('cms_template', $templateid)) {
    $show = node_load($fc->field_cms_show[LANGUAGE_NONE][0]['target_id']);
    if (!empty($cms_template->field_cms_template_offset[LANGUAGE_NONE])) {
      // fields in arings that not exist in cm_show default
      $show_length = NULL;
      drupal_alter('cm_series_duration', $show_length, $show);
      $channel = NULL;
      drupal_alter('cm_series_channel', $channel, $show);

      foreach ($cms_template->field_cms_template_offset[LANGUAGE_NONE] as $offset) {
        // Create starttime for airing in UTC time from offset in user timezone.
        $date = new DateTime($fc->field_cms_start_date[LANGUAGE_NONE][0]['value'], new DateTimeZone(drupal_get_user_timezone()));
        $date->setTimezone(new DateTimeZone('UTC'));
        $date->modify('+' . $offset['value'] . ' sec');
        $date_str['value'] = $date->format('Y-m-d H:i:s');

        $date->modify('+' . $show_length . ' sec');
        $date_str['value2'] = $date->format('Y-m-d H:i:s');
        $values = array(
          'type' => 'airing',
          'field_airing_title' => array(
            LANGUAGE_NONE => array(
              array(
                'value' => $show->title,
                'format' => NULL,
                'safe_value' => strip_tags($show->title),
              )
            )
          ),
          // DONE: fix for okv field_okv_duration
          'field_airing_duration' => array(LANGUAGE_NONE => array(array('value' => $show_length))),
          'field_airing_channel' => array(LANGUAGE_NONE => array(array('tid' => $channel))),
          'field_airing_date' => array(LANGUAGE_NONE => array($date_str)),
          'field_airing_show_ref' => array(LANGUAGE_NONE => array(array('target_id' => $show->nid))),
        );
        $airing = entity_create('airing', $values);
        $airing->save();
        $fc->field_cms_airings[LANGUAGE_NONE][] = array('target_id' => $airing->airing_id);
        drupal_set_message(t('Airgin: @cm_show_title, @airing_time1 (UTC) to @airing_time2 (UTC) created', array(
          '@cm_show_title' => $show->title,
          '@airing_time1' => $date_str['value'],
          '@airing_time2' => $date_str['value2'],
        )));
      }
      $fc->save();
    }
  }
}

function cm_series_delete_arings($form, &$form_state, $node) {
  $form['shows_markup_start'] = array('#markup' => '<div class="form-item title"><h1>Shows with arings</h1><p>With selected shows, all arings will be deleted</p></div>');
  $form['table'] = array(
    '#tree' => TRUE,
    '#type' => 'table',
    '#header' => array(
      '' => array('class' => array('select-all')), // For selecting items
      'Show Titel',
      'Start date',
    ),
  );
  // For selecting items
  drupal_add_js('misc/tableselect.js');

  if (!empty($node->field_cm_series_episodes[LANGUAGE_NONE])) {
    foreach ($node->field_cm_series_episodes[LANGUAGE_NONE] as $key => $value) {
      $fc = entity_load_single('field_collection_item', $value['value']);

      if (empty($fc->field_cms_airings[LANGUAGE_NONE])) {
        continue;
      }
      $show = node_load($fc->field_cms_show[LANGUAGE_NONE][0]['target_id']);
      $form['table'][$fc->item_id]['choosen'] = array(
        '#type' => 'checkbox',
        '#return_value' => $fc->item_id,
        '#default_value' => $fc->item_id,
      );
      $form['table'][$fc->item_id]['title'] = array('#markup' => $show->title);
      $date = new DateTime($fc->field_cms_start_date[LANGUAGE_NONE][0]['value'], new DateTimeZone(drupal_get_user_timezone()));
      $form['table'][$fc->item_id]['date'] = array('#markup' => $date->format('Y-m-d'));

    }
  }
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete Arings'),
    '#submit' => array('cm_series_delete_arings_action'),
    '#validate' => array('cm_series_delete_arings_validate'),
  );
  return $form;
}

function cm_series_delete_arings_action($form, &$form_state) {
  foreach ($form_state['values']['table'] as $fid) {
    if (!empty($fid['choosen'])) {
      _cms_delete_arings($fid['choosen']);
    }
  }
}

function cm_series_delete_arings_validate($form, &$form_state) {

}

function _cms_delete_arings($fid) {
  $fc = entity_load_single('field_collection_item', $fid);
  if (!empty($fc->field_cms_airings[LANGUAGE_NONE])) {
    foreach ($fc->field_cms_airings[LANGUAGE_NONE] as $airing) {
      entity_delete('airing', $airing['target_id']);
    }
    unset($fc->field_cms_airings[LANGUAGE_NONE]);
    $fc->save();
  }
}
