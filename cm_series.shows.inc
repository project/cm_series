<?php
/**
 * @file
 * CM Series shows.
 */

function cm_series_create_shows($form, &$form_state, $node) {
  $form_state['node'] = $node;
  $form['#parents'] = array();
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Show name'),
    '#default_value' => !empty($node->title) ? $node->title : '',
    '#weight' => 10,
    '#required' => TRUE,
  );
  $form['start_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Start number'),
    '#weight' => 20,
    '#description' => 'The number of the first episode created this round, must be an integer.',
    '#required' => FALSE,
  );

  // get common fields and create form elements
  $cm_show = field_info_instances('node', 'cm_show');
  $cm_series = field_info_instances('node', 'cm_series');
  $common_fields = array_intersect(array_keys($cm_series), array_keys($cm_show));
  $form['common_fields'] = array(
    '#tree' => TRUE,
    '#weight' => 25,
  );
  foreach ($common_fields as $fieldname) {
    $field = field_info_field($fieldname);
    $instance = $cm_show[$fieldname];
    $items = $node->{$fieldname}[LANGUAGE_NONE];
    $new_field = field_default_form('node', $node, $field, $instance, LANGUAGE_NONE, $items, $form, $form_state);
    $form['common_fields'] += $new_field;
  }

  $form['days'] = array(
    '#title' => t('Choose days'),
    '#type' => 'checkboxes',
    '#options' => array(
      'Mon' => t('Monday'),
      'Tue' => t('Tuesday'),
      'Wed' => t('Wednesday'),
      'Thu' => t('Thursday'),
      'Fri' => t('Friday'),
      'Sat' => t('Saturday'),
      'Sun' => t('Sunday'),
    ),
    '#required' => TRUE,
    '#weight' => 40,
  );

  // Get all templates
  $query = db_select('cms_template', 't');
  $query->fields('t', array(
    'id',
    'label'
  ));
  $result = $query->execute();
  $options = $result->fetchAllKeyed();
  $form['template'] = array(
    '#title' => t('Choose template'),
    '#type' => 'select',
    '#options' => $options,
    '#required' => TRUE,
    '#weight' => 50,
  );

  $form['start_date'] = array(
    '#title' => t('Start date for generated shows'),
    '#type' => 'date_popup',
    '#date_timezone' => date_default_timezone(),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-3:+3',
    '#required' => TRUE,
    '#weight' => 60,
  );

  $form['stop_date'] = array(
    '#title' => t('Stop date for generated shows'),
    '#type' => 'date_popup',
    '#date_timezone' => date_default_timezone(),
    '#date_format' => 'Y-m-d',
    '#date_year_range' => '-3:+3',
    '#required' => TRUE,
    '#weight' => 70,
  );

  $form['actions'] = array('#weight' => 80);
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create Shows'),
    '#weight' => 10,
    '#submit' => array('cm_series_create_shows_action'),
    '#validate' => array('cm_series_create_shows_validate'),
  );
  return $form;
}

function cm_series_create_shows_action($form, &$form_state) {
  global $language;
  $values = $form_state['values'];
  $date = $values['start_date'];
  $stop_date = $values['stop_date'];
  $nr = (int) $values['start_number'];
  while (strtotime($date) <= strtotime($stop_date)) {
    $day = date('D', strtotime($date));
    $days = array_values($values['days']);
    if (in_array($day, $days, TRUE)) {
      // Create show.
      $show = new stdClass();
      $show->title = $values['title'] . ($nr > 0 ? ' ' . $nr : '');
      ($nr > 0 ? $nr++ : '');

      $show->type = 'cm_show';
      node_object_prepare($show);
      $show->language = $language->language;

      // For all common elements...
      if (!empty($form_state['values']['common_fields'])) {
        foreach ($form_state['values']['common_fields'] as $key => $value) {
          $show->{$key} = $value;
        }
      }
      // Relate show to this series.
      $show->field_cm_show_series[LANGUAGE_NONE][0]['target_id'] = $form_state['node']->nid;
      node_save($show);

      $cms = node_load($form_state['node']->nid);
      // Add field_colletion to node
      $fc_values = array(
        'field_name' => 'field_cm_series_episodes',
        'field_cms_show' => array(LANGUAGE_NONE => array(array('target_id' => $show->nid))),
        'field_cms_template' => array(LANGUAGE_NONE => array(array('target_id' => $values['template']))),
        'field_cms_start_date' => array(LANGUAGE_NONE => array(array('value' => date("Y-m-d", strtotime($date))))),
        'field_cms_airings' => array(),
      );
      $fc = entity_create('field_collection_item', $fc_values);
      $fc->setHostEntity('node', $cms);
      node_save($cms);
    }
    // Next day.
    $date = date("Y-m-d", strtotime("+1 day", strtotime($date)));
  }
}

function cm_series_create_shows_validate($form, &$form_state) {
  if ($form_state['values']['start_date'] >= $form_state['values']['stop_date']) {
    form_set_error('start_date', '"Start date" must be smaller then "Stop date".');
    form_set_error('stop_date');
  }
  if (!preg_match('/^[1-9][0-9]{0,15}$/', $form_state['values']['start_number']) && !empty($form_state['values']['start_number'])) {
    form_set_error('start_number', 'The number of the first episode must be an integer or empty');
  }
}

function cm_series_delete_shows($form, &$form_state, $node) {
  $form_state['node'] = $node;
  $form['shows_markup_start'] = array('#markup' => '<div class="form-item title"><h1>All Shows in ' . $node->title . '</h1><p>Shows and related airings will be deleted<br>Show marked with * have arings<p></div>');
  $form['table'] = array(
    '#tree' => TRUE,
    '#type' => 'table',
    '#header' => array(
      '' => array('class' => array('select-all')), // For selecting items
      'Show Titel',
      'Start date',
      '',
    ),
  );
  // For selecting items
  drupal_add_js('misc/tableselect.js');

  if (!empty($node->field_cm_series_episodes[LANGUAGE_NONE])) {
    foreach ($node->field_cm_series_episodes[LANGUAGE_NONE] as $key => $value) {
      $fc = entity_load_single('field_collection_item', $value['value']);
      $have_arings = '';
      if (!empty($fc->field_cms_airings[LANGUAGE_NONE])) {
        $have_arings = ' *';
      }
      $show = node_load($fc->field_cms_show[LANGUAGE_NONE][0]['target_id']);
      $form['table'][$fc->item_id]['choosen'] = array(
        '#type' => 'checkbox',
        '#return_value' => $fc->item_id,
        '#default_value' => $fc->item_id,
      );
      $date = new DateTime($fc->field_cms_start_date[LANGUAGE_NONE][0]['value'], new DateTimeZone(drupal_get_user_timezone()));
      $form['table'][$fc->item_id]['title'] = array('#markup' => $show->title . $have_arings);
      $form['table'][$fc->item_id]['date'] = array('#markup' => $date->format('Y-m-d'));
      $form['table'][$fc->item_id]['delete_link'] = array('#markup' => l('Delete show' . ($have_arings == '' ? '' : ' and arings'), 'node/' . $node->nid . '/delete-cm-series-fc/' . $fc->item_id . '/delete'));
    }
  }
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete selected Shows'),
    '#submit' => array('cm_series_delete_shows_action'),
    '#validate' => array('cm_series_delete_shows_validate'),
  );
  return $form;

}

function cm_series_delete_shows_action($form, &$form_state) {
  foreach ($form_state['values']['table'] as $fid) {
    if (!empty($fid['choosen'])) {
      _cms_delete_arings($fid['choosen']);
      _cms_delete_shows($fid['choosen']);
    }
  }
}

function cm_series_delete_shows_validate($form, &$form_state) {

}

function _cms_delete_shows($fid) {
  $fc = entity_load_single('field_collection_item', $fid);
  if (!empty($fc->field_cms_show[LANGUAGE_NONE][0]['target_id'])) {
    node_delete($fc->field_cms_show[LANGUAGE_NONE][0]['target_id']);
    $fc->delete();
  }
}

/**
 * Delete confirmation form.
 */
function cm_series_fc_delete($form, &$form_state, $node, $fid) {
  $form_state['node'] = $node;
  $form_state['fid'] = $fid;
  return confirm_form($form, t('Are you sure you want to this %title?', array('%title' => 'show')), 'node/' . $node->nid . '/delete-shows', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Delete form submit handler.
 */
function cm_series_fc_delete_submit($form, &$form_state) {
  _cms_delete_shows($form_state['fid']);
  drupal_set_message(t('The show have been deleted.'));
  $form_state['redirect'] = 'node/' . $form_state['node']->nid . '/delete-shows';
}
